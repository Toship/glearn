/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glearn;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;

/**
 *
 * @author Toship
 */
public class AttendanceGet implements Runnable{

    private String url;
    private Map<String, String> cookie;
    private ArrayList<Attendance> attainList = new ArrayList<Attendance>();
    private Float attainper = null;
    
    public AttendanceGet(String url, Map<String, String> cookie){
        this.url = url + "&view=5";
        this.cookie = cookie;
    }
    
    @Override
    public void run() {
        try {
            Document attainPage = Jsoup.connect(url).cookies(cookie).get();
            Elements table = attainPage.select("table[class=generaltable attwidth boxaligncenter]")
                    .select("tbody").select("tr");
            String per = attainPage.select("table[class=attlist]")
                    .select("tr[class=normal]").get(2).select("td[class=cell c1 lastcol]")
                    .text();
            attainper = Float.parseFloat(per.substring(0, per.length() - 1));
            System.out.println(attainper + "");
            //int i = 0;
            for(Element row:table){
                String date = row.select("td[class=cell c2]").text();
                String time = row.select("td[class=cell c3]").text();
                String status = row.select("td[class=cell c5]").text();
                attainList.add(new Attendance(date, time, status));
                //System.out.println(attainList.get(i).getDate());
                //i++;
            }
            
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }
    
    public ArrayList<Attendance> getAttendance(){
        return attainList;
    }
    
    public Float getattainper(){
        return attainper;
    }
    
}
