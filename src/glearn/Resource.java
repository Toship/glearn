/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glearn;

import java.util.*;

/**
 *
 * @author Toship
 */
public class Resource {
    
    private String heading;
    private LinkedHashMap<String, LinkedHashMap<String, String>> files = new LinkedHashMap<String, LinkedHashMap<String, String>>();
    
    public Resource(String heading, LinkedHashMap<String, LinkedHashMap<String, String>> files){
        this.heading = heading;
        this.files = files;
    }
    
    public String getHeading(){
        return heading;
    }
    
    public LinkedHashMap<String, LinkedHashMap<String, String>> getResources(){
        return files;
    }
    
}
