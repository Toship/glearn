/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glearn;

import java.util.ArrayList;

/**
 *
 * @author Toship
 */
class Course {
    
    private String courseName = null;
    private ArrayList<Attendance> attendance = new ArrayList<Attendance>();
    private Float perAttendance = null;
    
    public Course(String courseName, Float perAttendance, ArrayList<Attendance> attendance){
        this.courseName = courseName;
        this.perAttendance = perAttendance;
        this.attendance = attendance;
    }
    
    public String getName(){
        return this.courseName;
    }
    
    public ArrayList<Attendance> getAttendance(){
        return this.attendance;
    }
    
    public Float getPerAtten(){
        return this.perAttendance;
    }
}
