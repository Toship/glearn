/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glearn;

/**
 *
 * @author Toship
 */
public class Attendance {
    
    private String time;
    private String status;
    private String date;
    
    public Attendance(String date, String time, String status){
        this.date = date;
        this.time = time;
        this.status = status;
    }
    
    public String getTime(){
        return this.time;
    }
    
    public String getDate(){
        return this.date;
    }
    
    public String getStatus(){
        return this.status;
    }
}
