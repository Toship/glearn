//This should be courseRetriver

package glearn;

import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Toship
 */
public class AttendanceRetriver implements Runnable{
    
    private Map<String, String> cookie;
    private String url;
    private String courseName;
    private AttendanceGet atg = null;
    private Course course = null;
    private ArrayList<Resource> resources = new ArrayList<Resource>();
    private Resource r1;
    
    public AttendanceRetriver(String link, Map<String, String> cookie, String courseName){
        this.url = link;
        this.cookie = cookie;
        this.courseName = courseName;
    }
    
    @Override
    public void run() {
        try {
                String attainUrl = null;
                Document page = Jsoup.connect(url).cookies(cookie).get();
                Element link = page.select("li[class=activity attendance modtype_attendance]").select("a").first();
                attainUrl = link.attr("href").toString();
                atg = new AttendanceGet(attainUrl, cookie);
                Thread a = new Thread(atg);
                a.start();
                //System.out.println(attainUrl);
                
                
                LinkedHashMap<String, LinkedHashMap<String, String>> r2 = new LinkedHashMap<String, LinkedHashMap<String, String>>();
                
                int i = 0;
                while(true){
                    String name;
                    String reslink;
                    String heading;
                    LinkedHashMap<String, String> resource = new LinkedHashMap<String, String>();
                    Elements crsres = page.select("li[id=section-"+i+"]");
                    Elements rows = crsres.select("li[class=activity folder modtype_folder], li[class=activity resource modtype_resource]");
                    if(rows.isEmpty())
                        break;
                    heading = crsres.select("h3").text();
                    //System.out.println(heading);
                    for(Element one:rows){
                        Elements filenames = one.select("li").select("span[class=fp-filename-icon]");
                        if(!filenames.select("span[class=fp-filename]").isEmpty()){
                            for(Element s:filenames){
                                name = s.text();
                                reslink = s.select("a").attr("href");
                                resource.put(name, reslink);
                                //System.out.println(name+" - "+reslink);
                            }
                        }
                        else{
                            name = one.text().replace(" File", "").replace(" Folder", "");
                            reslink = one.select("a").attr("href");
                            resource.put(name, reslink);
                            //System.out.println(name+" - "+reslink);
                        }
                    }
                    r2.put(heading, resource);
                    //resources.add(new Resource(heading, resource));
                    //System.out.println(resources.get(i).getHeading());
                    i++;
                    
                }
                
                r1 = new Resource("new", r2);
               // System.out.println(r2);
                
                
                while(a.isAlive())
                    Thread.sleep(1000);
                course = new Course(courseName, atg.getattainper(), atg.getAttendance());
                
            } catch (IOException ex) {
                System.out.println(ex);
            } catch (InterruptedException ex) {
            Logger.getLogger(AttendanceRetriver.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Course getcourse(){
        return course;
    }
    
}
