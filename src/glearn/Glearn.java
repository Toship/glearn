/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glearn;

import java.io.*;
import java.util.*;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Toship
 */
public class Glearn {
    
    private static String mUsername = "1210316057";
    private static String mPassword = "75135984267913";
    private static Map<String, String> cookie;
    private static Map<String, String> distlinks = new HashMap<String,String>();
    private static Document homepage;
    private final static String url = "https://glearn.gitam.edu/login/index.php";
    
    
    public static boolean login() throws IOException{
        Connection.Response res = Jsoup.connect(url).data("username", mUsername)
                .data("password", mPassword).method(Connection.Method.POST)
                .execute();
        homepage = res.parse();
        if(homepage.title().equals("Dashboard")){
            cookie = res.cookies();
            return true;
        }
        else
            return false;
    }
    
    public static void getCourseLinks(){
        Elements els = homepage.select("div[class=visible-desktop]");
        for(Element link : els){
            distlinks.put(link.select("a[href]").attr("href"), link.text());
        }
        String img = homepage.select("header[id=page-header]").select("img[src]")
                .attr("src");
        System.out.println(img);
        /*
        *
        *Printing Course links for debugging
        *
        for(Map.Entry<String,String> entry : distlinks.entrySet()){
            System.out.println(entry);
        }
        */
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        //mUsername = br.readLine();
        //mPassword = br.readLine();
        if(login())
            System.out.println("Login Successful!");
        else{
            System.out.println("Login Failed!");
            return;
        }
        getCourseLinks();
        DataRetriver data = new DataRetriver(distlinks, cookie);
        Thread a = new Thread(data);
        a.start();
        while(a.isAlive())
            Thread.sleep(1000);
    }
    
}
