//This should be main


package glearn;

import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Toship
 */
public class DataRetriver implements Runnable{

    private Map<String, String> links;
    private Map<String, String> cookie;
    private Thread[] threads;
    private AttendanceRetriver[] attendanceretriver;
    private ArrayList<Course> courseList = new ArrayList<Course>();
    
    public DataRetriver(Map<String,String> links, Map<String,String> cookie){
        this.links = links;
        this.cookie = cookie;
        this.threads = new Thread[links.size()];
        this.attendanceretriver = new AttendanceRetriver[links.size()];
    }
    
    @Override
    public void run() {
        int i = 0;
        for(Map.Entry<String, String> link:links.entrySet()){
            attendanceretriver[i] = new AttendanceRetriver(link.getKey(), cookie, link.getValue());
            threads[i] = new Thread(attendanceretriver[i]);
            threads[i].start();
            i++;
        }
        
        while(!iscompleted())
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(DataRetriver.class.getName()).log(Level.SEVERE, null, ex);
            }
        for(int j=0;j<links.size();j++){
            courseList.add(attendanceretriver[j].getcourse());
           // System.out.println(courseList.get(j).getPerAtten());
        }
    }
    
    public boolean iscompleted(){
        for(int i=0;i<links.size();i++)
            if(threads[i].isAlive())
                return false;
        return true;
    }
    
}
